#include <stdio.h>
#include <stdlib.h>

#include <wlr/render/interface.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/util/log.h>

#include <shell/xdg.h>
#include <output/renderer.h>

void render_layer_surface(
	struct wlr_surface *surface,
	struct wlr_output *output,
	struct wlr_renderer *renderer,
	struct timespec now
) {
	/* Get the surface's texture */
	struct wlr_texture *texture = wlr_surface_get_texture(surface);
	if(texture == NULL) {
		/* Don't render non-existent texture */
		return;
	}

	/* Create a render box with dimensions */
	struct wlr_box render_box = {
		.x = 0, .y = 0,
		.width = surface->current.width,
		.height = surface->current.height,
	};

	/* Create a box projection matrix */
	float matrix[16];
	wlr_matrix_project_box(
		matrix,
		&render_box,
		surface->current.transform,
		0,
		output->transform_matrix
	);

	/* Render the surface */
	wlr_render_texture_with_matrix(renderer, texture, matrix, 1);

	/* Inform the surface that its frame has been rendered */
	wlr_surface_send_frame_done(surface, &now);
}

void render_xdg_surface(
	struct wlr_surface *surface,
	int x,
	int y,
	void *data
) {
	struct render_xdg_data *render_data = data;

	/* Get the surface's texture */
	struct wlr_texture *texture = wlr_surface_get_texture(surface);
	if(texture == NULL) {
		/* Don't render non-existent texture */
		return;
	}

	/* Create a render box with dimensions */
	struct wlr_box render_box = {
		.x = x + render_data->view->x, .y = y + render_data->view->y,
		.width = surface->current.width,
		.height = surface->current.height,
	};

	/* Create a box projection matrix */
	float matrix[16];
	wlr_matrix_project_box(
		matrix,
		&render_box,
		surface->current.transform,
		0,
		render_data->output->transform_matrix
	);

	/* Render the surface */
	wlr_render_texture_with_matrix(render_data->renderer, texture, matrix, 1);

	/* Inform the surface that its frame has been rendered */
	wlr_surface_send_frame_done(surface, &render_data->now);
}
