#include <stdio.h>
#include <stdlib.h>

#include <wlr-layer-shell-unstable-v1-protocol.h>
#include <wlr/render/interface.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/util/log.h>

#include <shell/server.h>
#include <shell/layer.h>
#include <shell/xdg.h>
#include <output/output.h>
#include <output/renderer.h>

static void output_render_layer(
	struct wlc_output *output,
	struct wlr_output *wlr_output,
	enum zwlr_layer_shell_v1_layer layer,
	struct timespec now
) {
	struct wlc_layer_surface *layer_surface;

	wl_list_for_each_reverse(
		layer_surface,
		&output->server->layer_shell->surfaces[layer],
		link
	) {

		if (layer_surface->surface->mapped) {
			render_layer_surface(
				layer_surface->surface->surface,
				wlr_output,
				output->server->renderer,
				now
			);
		}
	}
}

void output_frame_notify(struct wl_listener *listener, void *data) {
	struct wlc_output *output = wl_container_of(listener, output, frame);
	struct wlr_output *wlr_output = data;

	/* Record start time of frame */
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	/* Attach renderer to output */
	wlr_output_attach_render(wlr_output, NULL);

	wlr_renderer_begin(output->server->renderer, wlr_output->width, wlr_output->height);

	/* Render top layers */
	output_render_layer(output, wlr_output, ZWLR_LAYER_SHELL_V1_LAYER_OVERLAY, now);
	output_render_layer(output, wlr_output, ZWLR_LAYER_SHELL_V1_LAYER_TOP, now);

	/* Render XDG views */
	struct wlc_view *view;
	/* Iterate backwards to render bottom to top */
	wl_list_for_each_reverse(view, &output->server->xdg_shell->views, link) {
			/* Collect render data */
		struct render_xdg_data render_data = {
			.output = wlr_output,
			.renderer = output->server->renderer,
			.view = view,
			.now = now
		};

		/* Render view's main surface and all of its subsurfaces */
		wlr_xdg_surface_for_each_surface(view->xdg_surface,
				render_xdg_surface, &render_data);
	}

	/* Render bottom layers */
	output_render_layer(output, wlr_output, ZWLR_LAYER_SHELL_V1_LAYER_BOTTOM, now);
	output_render_layer(output, wlr_output, ZWLR_LAYER_SHELL_V1_LAYER_BACKGROUND, now);

	/* Render software cursors if needed */
	wlr_output_render_software_cursors(output->wlr_output, NULL);

	wlr_renderer_end(output->server->renderer);
	wlr_output_commit(wlr_output);
}

void output_new_notify(struct wl_listener *listener, void *data) {
	struct wlc_server *server = wl_container_of(listener, server, new_output);
	struct wlr_output *wlr_output = data;

	/* Set the last available mode if modes are available */
	if(!wl_list_empty(&wlr_output->modes)) {
		struct wlr_output_mode *mode = wl_container_of(wlr_output->modes.prev, mode, link);
		wlr_output_set_mode(wlr_output, mode);
	}

	/* Allocate an output struct */
	struct wlc_output *output = calloc(1, sizeof(struct wlc_output));
	output->server = server;
	output->wlr_output = wlr_output;
	/* Register frame signal */
	output->frame.notify = output_frame_notify;
	wl_signal_add(&wlr_output->events.frame, &output->frame);
	/* Register output removal signal */
	output->destroy.notify = output_destroy_notify;
	wl_signal_add(&wlr_output->events.destroy, &output->destroy);

	/* Add output to the list */
	wl_list_insert(&server->outputs, &output->link);

	/* Set active output */
	server->active_output = output;

	/* Add output to output layout */
	wlr_output_layout_add_auto(server->output_layout, wlr_output);
}

void output_destroy_notify(struct wl_listener *listener, void *data) {
	struct wlc_output *output = wl_container_of(listener, output, destroy);

	wl_list_remove(&output->link);
	wl_list_remove(&output->destroy.link);
	wl_list_remove(&output->frame.link);
}