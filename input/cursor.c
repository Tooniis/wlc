#include <stdio.h>
#include <stdlib.h>

#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/util/log.h>

#include <shell/xdg.h>
#include <input/input.h>
#include <input/cursor.h>

static void cursor_process_motion(struct wlc_cursor *cursor, uint32_t time_msec) {
	struct wlc_view *view = NULL;
	struct wlr_surface *surface = NULL;
	bool surface_changed = false;
	double x = 0, y = 0;
	double sx, sy;

	wlr_log(
		WLR_LOG_IMPORTANCE_LAST,
		"Cursor moved to (%f, %f)",
		cursor->wlr_cursor->x,
		cursor->wlr_cursor->y
	);

	/* Get view under cursor if a xdg shell is assigned to the cursor */
	if(cursor->xdg_shell) {
		view = xdg_shell_view_at_point(
			cursor->xdg_shell,
			cursor->wlr_cursor->x,
			cursor->wlr_cursor->y,
			&surface,
			&sx, &sy
		);
	}

	if(view != cursor->current_view) {
		/* Keep track of current view under cursor */
		cursor->current_view = view;
		wlr_log(WLR_DEBUG, "View under cursor changed to 0x%lx", view);
	}

	if(surface != cursor->current_surface) {
		surface_changed = true;
		/* Keep track of current surface under cursor */
		cursor->current_surface = surface;
		wlr_log(WLR_DEBUG, "Surface under cursor changed to 0x%lx", surface);
	}

	if(surface) {
		x = cursor->wlr_cursor->x - (view->x + surface->sx);
		y = cursor->wlr_cursor->y - (view->y + surface->sy);

		/* Notify seat about cursor entrance */
		wlr_seat_pointer_notify_enter(
			cursor->input->seat,
			surface,
			sx, sy
		);

		if(!surface_changed) {
			/* Notify seat about cursor motion */
			wlr_seat_pointer_notify_motion(
				cursor->input->seat,
				time_msec,
				sx, sy
			);
		}

		return;
	}

	/* Set default cursor image */
	wlr_xcursor_manager_set_cursor_image(
		cursor->cursor_manager,
		"left_ptr",
		cursor->wlr_cursor
	);
}

static void cursor_motion_notify(struct wl_listener *listener, void *data) {
	struct wlc_cursor *cursor = wl_container_of(listener, cursor, motion);
	struct wlr_event_pointer_motion *event = data;

	wlr_cursor_move(cursor->wlr_cursor, event->device, event->delta_x, event->delta_y);
	cursor_process_motion(cursor, event->time_msec);
}

static void cursor_motion_absolute_notify(struct wl_listener *listener, void *data) {
	struct wlc_cursor *cursor = wl_container_of(listener, cursor, motion_absolute);
	struct wlr_event_pointer_motion_absolute *event = data;

	wlr_cursor_warp_absolute(cursor->wlr_cursor, event->device, event->x, event->y);
	cursor_process_motion(cursor, event->time_msec);
}

static void cursor_button_notify(struct wl_listener *listener, void *data) {
	struct wlc_cursor *cursor = wl_container_of(listener, cursor, button);
	struct wlr_event_pointer_button *event = data;

	/* Emit focus change request signal if view under cursor changed since last click */
	if(cursor->xdg_shell->focused_view != cursor->current_view) {
		wl_signal_emit(&cursor->events.request_focus_change, cursor->current_view);
	}

	/* Notify seat about the button event */
	wlr_seat_pointer_notify_button(
		cursor->input->seat,
		event->time_msec,
		event->button,
		event->state
	);
}

static void cursor_frame(struct wl_listener *listener, void *data) {
	struct wlc_cursor *cursor = wl_container_of(listener, cursor, frame);

	/* Notify seat about the frame event */
	wlr_seat_pointer_notify_frame(cursor->input->seat);
}

struct wlc_cursor* cursor_create(struct wlc_input *input) {
	struct wlc_cursor *cursor = calloc(1, sizeof(struct wlc_cursor));

	/* Remember parent input seat */
	cursor->input = input;

	/* Create a cursor */
	cursor->wlr_cursor = wlr_cursor_create();

	/* Create a cursor manager */
	cursor->cursor_manager = wlr_xcursor_manager_create(NULL, 24);
	wlr_xcursor_manager_load(cursor->cursor_manager, 1);

	/* Initialize focus change request signal */
	wl_signal_init(&cursor->events.request_focus_change);

	/* Register motion signals */
	cursor->motion.notify = cursor_motion_notify;
	wl_signal_add(&cursor->wlr_cursor->events.motion, &cursor->motion);

	cursor->motion_absolute.notify = cursor_motion_absolute_notify;
	wl_signal_add(&cursor->wlr_cursor->events.motion_absolute, &cursor->motion_absolute);

	/* Register button signal */
	cursor->button.notify = cursor_button_notify;
	wl_signal_add(&cursor->wlr_cursor->events.button, &cursor->button);

	/* Register frame signal */
	cursor->frame.notify = cursor_frame;
	wl_signal_add(&cursor->wlr_cursor->events.frame, &cursor->frame);

	return cursor;
}