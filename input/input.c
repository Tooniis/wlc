#include <stdio.h>
#include <stdlib.h>

#include <wlr/backend.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/util/log.h>

#include <shell/server.h>
#include <input/input.h>
#include <input/cursor.h>
#include <input/keyboard.h>

static void input_request_cursor(struct wl_listener *listener, void *data) {
	struct wlc_input *input = wl_container_of(listener, input, request_cursor);
	struct wlr_seat_pointer_request_set_cursor_event *event = data;
	struct wlr_seat_client *focused_client = input->seat->pointer_state.focused_client;

	wlr_log(WLR_DEBUG, "Cursor request received");

	wlr_cursor_set_surface(
		input->cursor->wlr_cursor,
		event->surface,
		event->hotspot_x, event->hotspot_y
	);
}

static void input_new_keyboard(struct wlc_input *input, struct wlr_input_device *input_device) {
	struct wlc_keyboard *keyboard;

	/* Create a keyboard */
	keyboard = keyboard_create(input, input_device);

	/* Set seat keyboard */
	wlr_seat_set_keyboard(input->seat, input_device);

	/* Add created keyboard to list of keyboards */
	wl_list_insert(&input->keyboards, &keyboard->link);
}

static void input_new_notify(struct wl_listener *listener, void *data) {
	struct wlc_input *input = wl_container_of(listener, input, new_input);
	struct wlr_input_device *device = data;
	uint32_t capabilities = WL_SEAT_CAPABILITY_POINTER;

	wlr_log(WLR_INFO, "New Input");

	switch(device->type) {
		case WLR_INPUT_DEVICE_POINTER:
			wlr_cursor_attach_input_device(input->cursor->wlr_cursor, device);
			wlr_log(WLR_INFO, "Attached pointer");
			break;
		case WLR_INPUT_DEVICE_KEYBOARD:
			input_new_keyboard(input, device);
			break;
		default:;
	}

	if (!wl_list_empty(&input->keyboards)) {
		capabilities |= WL_SEAT_CAPABILITY_KEYBOARD;
	}
	wlr_seat_set_capabilities(input->seat, capabilities);
}

struct wlc_input* input_create(struct wlc_server *server, struct wl_display *display, const char* seat_name) {
	struct wlc_input *input = calloc(1, sizeof(struct wlc_input));

	/* Remember parent server */
	input->server = server;

	/* Create a seat */
	input->seat = wlr_seat_create(display, seat_name);

	/* Create a cursor */
	input->cursor = cursor_create(input);

	/* Register input signal */
	input->new_input.notify = input_new_notify;
	wl_signal_add(&input->server->backend->events.new_input, &input->new_input);

	/* Register cursor request signal */
	input->request_cursor.notify = input_request_cursor;
	wl_signal_add(&input->seat->events.request_set_cursor, &input->request_cursor);

	/* Initialize keyboard list */ 
	wl_list_init(&input->keyboards);

	return input;
}