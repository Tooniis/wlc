#include <stdio.h>
#include <stdlib.h>

#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/util/log.h>

#include <xkbcommon/xkbcommon.h>

#include <input/input.h>
#include <input/keyboard.h>

static void keyboard_modifiers_notify(struct wl_listener *listener, void *data) {
	struct wlc_keyboard *keyboard = wl_container_of(listener, keyboard, modifiers);

	wlr_log(WLR_DEBUG, "Modifier pressed");

	/* TODO: Handle shell-specific key binds */

	wlr_seat_set_keyboard(keyboard->input->seat, keyboard->device);
	wlr_seat_keyboard_notify_modifiers(
		keyboard->input->seat,
		&keyboard->device->keyboard->modifiers
	);
}

static void keyboard_key_notify(struct wl_listener *listener, void *data) {
	struct wlc_keyboard *keyboard = wl_container_of(listener, keyboard, key);
	struct wlr_event_keyboard_key *event = data;

	wlr_log(WLR_DEBUG, "Key pressed");

	wlr_seat_set_keyboard(keyboard->input->seat, keyboard->device);
	wlr_seat_keyboard_notify_key(
		keyboard->input->seat,
		event->time_msec,
		event->keycode,
		event->state
	);
}

struct wlc_keyboard* keyboard_create(
	struct wlc_input *input,
	struct wlr_input_device *input_device
) {
	struct wlc_keyboard *keyboard = calloc(1, sizeof(struct wlc_keyboard));

	/* Remember parent input seat */
	keyboard->input = input;
	/* Store input device of this keyboard */
	keyboard->device = input_device;

	/* Prepare XKB keymap */
	struct xkb_rule_names rules = { 0 };
	struct xkb_context *context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	struct xkb_keymap *keymap = xkb_map_new_from_names(
		context,
		&rules,
		XKB_KEYMAP_COMPILE_NO_FLAGS
	);

	/* Assign XKB keymap */
	wlr_keyboard_set_keymap(input_device->keyboard, keymap);
	xkb_keymap_unref(keymap);
	xkb_context_unref(context);

	/* Configure key repetition */
	/* TODO: Make the values configurable by the user */
	wlr_keyboard_set_repeat_info(input_device->keyboard, 40, 300);

	/* Register modifiers signal */
	keyboard->modifiers.notify = keyboard_modifiers_notify;
	wl_signal_add(&input_device->keyboard->events.modifiers, &keyboard->modifiers);

	/* Register modifiers signal */
	keyboard->key.notify = keyboard_key_notify;
	wl_signal_add(&input_device->keyboard->events.key, &keyboard->key);

	return keyboard;
}
