struct wlc_keyboard {
	struct wl_list link;
	struct wlc_input *input;
	struct wlr_input_device *device;

	struct wl_listener modifiers;
	struct wl_listener key;
};

struct wlc_keyboard* keyboard_create(
	struct wlc_input *input,
	struct wlr_input_device *input_device
);