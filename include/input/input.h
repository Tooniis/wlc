#include <wayland-server.h>
#include <wayland-server-protocol.h>

struct wlc_input {
	struct wlc_server *server;
	struct wlr_seat *seat;

	struct wlc_cursor *cursor;
	struct wl_list keyboards;

	struct wl_listener new_input;
	struct wl_listener request_cursor;
};

struct wlc_input* input_create(struct wlc_server *server, struct wl_display *display, const char* seat_name);