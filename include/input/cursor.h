#include <wlr/types/wlr_cursor.h>

struct wlc_cursor {
	struct wlr_cursor *wlr_cursor;
	struct wlc_input *input;
	struct wlr_xcursor_manager *cursor_manager;

	/* Used to manage views under the cursor */
	struct wlc_xdg_shell *xdg_shell;
	struct wlc_view *current_view;
	struct wlr_surface *current_surface;

	struct wl_listener motion;
	struct wl_listener motion_absolute;
	struct wl_listener button;

	struct wl_listener frame;

	struct {
		struct wl_signal request_focus_change;
	} events;
};

struct wlc_cursor* cursor_create(struct wlc_input *input);
