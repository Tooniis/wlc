struct wlc_output {
	/* Output */
	struct wlr_output *wlr_output;
	/* Server that owns the output */
	struct wlc_server *server;

	/* Signals */
	struct wl_listener destroy;
	struct wl_listener frame;

	struct wl_list link;
};

void output_frame_notify(struct wl_listener *listener, void *data);
void output_new_notify(struct wl_listener *listener, void *data);
void output_destroy_notify(struct wl_listener *listener, void *data);
