struct render_xdg_data {
	struct wlr_output *output;
	struct wlr_renderer *renderer;
	struct wlc_view *view;
	struct timespec now;
};

void render_layer_surface(
	struct wlr_surface *surface,
	struct wlr_output *output,
	struct wlr_renderer *renderer,
	struct timespec now
);

void render_xdg_surface(
	struct wlr_surface *surface,
	int x,
	int y,
	void *data
);