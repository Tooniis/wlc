#include <wlr/types/wlr_xdg_shell.h>

struct wlc_view {
	struct wlr_xdg_surface *xdg_surface;
	int x, y;

	struct wl_listener destroy;

	struct wl_list link;
};

struct wlc_xdg_shell {
	struct wlr_xdg_shell *wlr_xdg_shell;
	struct wl_display *display;
	struct wlc_cursor *cursor;
	struct wlc_input *input;

	struct wlc_view *focused_view;

	struct wl_listener new_surface;
	struct wl_listener focus_change_request;

	struct wl_list views;
};

struct wlc_view* xdg_shell_view_at_point(
	struct wlc_xdg_shell *xdg_shell,
	double x, double y,
	struct wlr_surface **surface,
	double *sx, double *sy
);

struct wlc_xdg_shell* xdg_shell_create(struct wl_display *display, struct wlc_input *input);
