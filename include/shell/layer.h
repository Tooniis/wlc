struct wlc_layer_shell;

struct wlc_layer_surface {
	struct wlr_layer_surface_v1 *surface;
	struct wlc_layer_shell *shell;

	struct wl_listener destroy;

	struct wl_list link;
};

struct wlc_layer_shell {
	struct wlr_layer_shell_v1 *shell;
	struct wl_display *display;
	struct wlc_server *server;

	struct wl_listener new_surface;

	struct wl_list surfaces[4];
};

struct wlc_layer_shell* layer_shell_create(struct wl_display *display, struct wlc_server *server);
