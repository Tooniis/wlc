struct wlc_server {
	struct wl_display *display;
	struct wl_event_loop *event_loop;
	struct wlr_backend *backend;
	struct wlr_output_layout *output_layout;
	struct wlc_output *active_output;
	struct wlr_renderer *renderer;
	struct wlc_input *input;

	struct wlc_xdg_shell *xdg_shell;
	struct wlc_layer_shell *layer_shell;

	struct wl_listener new_output;
	struct wl_list outputs;
};