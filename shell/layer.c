#include <stdio.h>
#include <stdlib.h>

#include <wlr-layer-shell-unstable-v1-protocol.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_layer_shell_v1.h>
#include <wlr/util/log.h>

#include <shell/server.h>
#include <shell/layer.h>
#include <output/output.h>

static void layer_shell_surface_destroy_notify(struct wl_listener *listener, void *data) {
	struct wlc_layer_surface *layer_surface = wl_container_of(listener, layer_surface, destroy);

	/* Remove layer surface from surfaces list */
	wl_list_remove(&layer_surface->link);

	/* Free memory */
	free(layer_surface);
}

static void layer_shell_surface_new_notify(struct wl_listener *listener, void *data) {
	struct wlc_layer_shell *shell = wl_container_of(listener, shell, new_surface);
	struct wlc_layer_surface *layer_surface = calloc(1, sizeof(struct wlc_layer_surface));
	int layer;
	int width = shell->server->active_output->wlr_output->width;
	int height = shell->server->active_output->wlr_output->height; 

	layer_surface->surface = data;
	layer_surface->shell = shell;
	layer = layer_surface->surface->client_pending.layer;

	wlr_layer_surface_v1_configure(layer_surface->surface, width, height);

	/* Register destroy signal */
	layer_surface->destroy.notify = layer_shell_surface_destroy_notify;
	wl_signal_add(&layer_surface->surface->events.destroy, &layer_surface->destroy);

	wl_list_insert(
		&shell->surfaces[layer],
		&layer_surface->link
	);
	wlr_log(
		WLR_DEBUG,
		"New surface inserted into layer %d",
		layer_surface->surface->client_pending.layer
	);
}

struct wlc_layer_shell* layer_shell_create(struct wl_display *display, struct wlc_server *server) {
	struct wlc_layer_shell *layer_shell = calloc(1, sizeof(struct wlc_layer_shell));

	layer_shell->display = display;
	layer_shell->shell = wlr_layer_shell_v1_create(display);
	layer_shell->server = server;

	/* Initialize surface lists */
	for (int i = 0; i < 4; i++)
		wl_list_init(&layer_shell->surfaces[i]);

	/* Register new surface signal */
	layer_shell->new_surface.notify = layer_shell_surface_new_notify;
	wl_signal_add(&layer_shell->shell->events.new_surface, &layer_shell->new_surface);

	return layer_shell;
}
