#include <stdio.h>
#include <stdlib.h>

#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/log.h>

#include <input/cursor.h>
#include <input/input.h>
#include <shell/xdg.h>

struct wlc_view* xdg_shell_view_at_point(
	struct wlc_xdg_shell *xdg_shell,
	double x, double y,
	struct wlr_surface **surface,
	double *sx, double *sy
) {
	struct wlc_view *view;
	wl_list_for_each(view, &xdg_shell->views, link) {
		*surface = wlr_xdg_surface_surface_at(
			view->xdg_surface,
			x - view->x, y - view->y,
			sx, sy
		);
		if (*surface) {
			return view;
		}
	}

	/* No view found at location */
	return NULL;
}

static void xdg_shell_change_focus(struct wlc_xdg_shell *xdg_shell, struct wlc_view *view) {
	struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(xdg_shell->input->seat);

	if(keyboard) {
		wlr_seat_keyboard_notify_enter(
			xdg_shell->input->seat,
			view->xdg_surface->surface,
			keyboard->keycodes,
			keyboard->num_keycodes,
			&keyboard->modifiers
		);
	}

	xdg_shell->focused_view = view;
}

static void xdg_shell_surface_destroy_notify(struct wl_listener *listener, void *data) {
	struct wlc_view *view = wl_container_of(listener, view, destroy);

	/* Remove view from list of views */
	wl_list_remove(&view->link);

	/* Free memory */
	free(view);
}

static void xdg_shell_surface_new_notify(struct wl_listener *listener, void *data) {
	struct wlc_xdg_shell *xdg_shell = wl_container_of(listener, xdg_shell, new_surface);
	struct wlr_xdg_surface *xdg_surface = data;

	wlr_log(WLR_INFO, "New xdg surface");

	/* Only make a view if the surface is a toplevel */
	if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL)
		return;

	/* Allocate a view for this surface */
	struct wlc_view *view =
		calloc(1, sizeof(struct wlc_view));

	view->xdg_surface = xdg_surface;
	view->x = 20;
	view->y = 20;

	/* Register view signals */
	view->destroy.notify = xdg_shell_surface_destroy_notify;
	wl_signal_add(&xdg_surface->events.destroy, &view->destroy);

	/* Activate view */
	wlr_xdg_toplevel_set_activated(view->xdg_surface, true);

	/* Focus view */
	xdg_shell_change_focus(xdg_shell, view);

	wl_list_insert(&xdg_shell->views, &view->link);
	wlr_log(WLR_INFO, "New surface added");
}

static void xdg_shell_focus_change_request_notify(struct wl_listener *listener, void *data) {
	struct wlc_xdg_shell *xdg_shell = wl_container_of(
		listener,
		xdg_shell,
		focus_change_request
	);
	struct wlc_view *view = data;

	xdg_shell_change_focus(xdg_shell, view);

	wlr_log(WLR_INFO, "Changed focus");
}

struct wlc_xdg_shell* xdg_shell_create(struct wl_display *display, struct wlc_input *input) {
	struct wlc_xdg_shell *xdg_shell = calloc(1, sizeof(struct wlc_xdg_shell));

	xdg_shell->wlr_xdg_shell = wlr_xdg_shell_create(display);
	xdg_shell->display = display;
	xdg_shell->input = input;
	xdg_shell->cursor = input->cursor;
	xdg_shell->cursor->xdg_shell = xdg_shell;
	xdg_shell->focused_view = NULL; 

	wl_list_init(&xdg_shell->views);

	/* Register new surface signal */
	xdg_shell->new_surface.notify = xdg_shell_surface_new_notify;
	wl_signal_add(&xdg_shell->wlr_xdg_shell->events.new_surface, &xdg_shell->new_surface);

	/* Register focus change request signal */
	xdg_shell->focus_change_request.notify = xdg_shell_focus_change_request_notify;
	wl_signal_add(&xdg_shell->cursor->events.request_focus_change, &xdg_shell->focus_change_request);

	return xdg_shell;
}