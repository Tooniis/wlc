#include <stdio.h>
#include <stdlib.h>

#include <wayland-server.h>
#include <wayland-server-protocol.h>
#include <wlr/backend.h>
#include <wlr/render/interface.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_surface.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/util/log.h>

#include <input/input.h>
#include <input/cursor.h>
#include <input/keyboard.h>
#include <output/output.h>
#include <shell/layer.h>
#include <shell/server.h>
#include <shell/xdg.h>

int main(int argc, char const *argv[]) {
	struct wlc_server server;

	wlr_log_init(WLR_DEBUG, NULL);

	/* Create a display */
	server.display = wl_display_create();

	/* Get an event loop */
	server.event_loop = wl_display_get_event_loop(server.display);

	/* Create a backend */
	server.backend = wlr_backend_autocreate(server.display);

	/* Get a renderer */
	server.renderer = wlr_backend_get_renderer(server.backend);
	/* Initialize display in renderer */
	wlr_renderer_init_wl_display(server.renderer, server.display);

	/* Create a compositor */
	wlr_compositor_create(server.display, server.renderer);

	/* Create a data device manager to handle clipboard */
	wlr_data_device_manager_create(server.display);

	server.output_layout = wlr_output_layout_create();

	/* Initialize output list */
	wl_list_init(&server.outputs);

	/* Register new output signal */
	server.new_output.notify = output_new_notify;
	wl_signal_add(&server.backend->events.new_output, &server.new_output);

	/* Create an input seat */
	server.input = input_create(&server, server.display, "seat0");

	/* Attach input seat's cursor to output layout */
	wlr_cursor_attach_output_layout(server.input->cursor->wlr_cursor, server.output_layout);

	/* Create a XDG shell */
	server.xdg_shell = xdg_shell_create(server.display, server.input);

	/* Create a layer shell */
	server.layer_shell = layer_shell_create(server.display, &server);

	/* Add a Unix socket to the display */
	const char *socket = wl_display_add_socket_auto(server.display);

	/* Start the backend */
	if(!wlr_backend_start(server.backend)) {
		fprintf(stderr, "Failed to start backend\n");
		wlr_backend_destroy(server.backend);
		wl_display_destroy(server.display);
		return 1;
	}

	/* Set WAYLAND_DISPLAY to the added socket */
	wlr_log(WLR_INFO, "Running compositor on wayland display '%s'\n", socket);
	setenv("WAYLAND_DISPLAY", socket, true);

	/* Enter the event loop */
	wl_display_run(server.display);

	/* Cleanup after quitting the loop */
	wl_display_destroy(server.display);

	return 0;
}
